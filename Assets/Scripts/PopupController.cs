﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupController : MonoBehaviour
{
    [SerializeField] Button closeButton;
    
    // Start is called before the first frame update
    void Start()
    {
        closeButton.onClick.AddListener(delegate
        {
            gameObject.SetActive(false);
        });
        
        Hide();
    }

    public void Show()
    {
        //TODO: Create show animation here
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        //TODO: Create hide animation here
        gameObject.SetActive(false);
    }
}
